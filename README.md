# LoginScreen
An app to show Login Screen with a customized background

-  Application built in Java
-  Clean, Simple and Minimalist design
-  Project Follows MVP architectural pattern
-  Uses Dagger2 for Dependency Injection
-  User Input validations
-  Includes Unit Test for validations


## App has 2 screens
- Login screen with email and password field
- Home screen showing the user a welcome text after successful login