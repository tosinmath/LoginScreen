package com.loginscreen;


import android.app.Application;

import com.loginscreen.dependencies.component.DaggerLoginComponent;
import com.loginscreen.dependencies.component.DaggerPreferenceComponent;
import com.loginscreen.dependencies.component.LoginComponent;
import com.loginscreen.dependencies.component.PreferenceComponent;
import com.loginscreen.dependencies.module.AppModule;
import com.loginscreen.dependencies.module.LoginModule;
import com.loginscreen.dependencies.module.PreferenceModule;

public class BaseApplication extends Application {

    private LoginComponent loginComponent;
    private PreferenceComponent preferenceComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        preferenceComponent = DaggerPreferenceComponent.builder()
                .appModule(new AppModule(this)).preferenceModule(new PreferenceModule()).build();

        loginComponent = DaggerLoginComponent.builder()
                .preferenceComponent(preferenceComponent).loginModule(new LoginModule()).build();
    }

    public LoginComponent getLoginComponent() {
        return loginComponent;
    }

    public PreferenceComponent getPreferenceComponent() {
        return preferenceComponent;
    }
}
