package com.loginscreen.dependencies.component;


import android.content.SharedPreferences;

import com.loginscreen.dependencies.module.AppModule;
import com.loginscreen.dependencies.module.PreferenceModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules={AppModule.class, PreferenceModule.class})
public interface PreferenceComponent {
    SharedPreferences sharedPreferences();
}
