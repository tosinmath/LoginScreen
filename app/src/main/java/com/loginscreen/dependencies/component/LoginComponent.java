package com.loginscreen.dependencies.component;


import com.loginscreen.dependencies.module.LoginModule;
import com.loginscreen.dependencies.scope.UserScope;
import com.loginscreen.ui.home.HomeActivity;
import com.loginscreen.ui.login.LoginActivity;


import dagger.Component;

@UserScope
@Component(dependencies = PreferenceComponent.class, modules = {LoginModule.class})
public interface LoginComponent {
    void inject(LoginActivity loginActivity);
    void inject(HomeActivity homeActivity);
}