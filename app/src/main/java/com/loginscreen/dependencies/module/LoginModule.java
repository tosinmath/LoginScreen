package com.loginscreen.dependencies.module;

import android.app.Application;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.loginscreen.interactor.LoginInteractor;
import com.loginscreen.interactor.LoginInteractorImpl;
import com.loginscreen.ui.home.HomePresenter;
import com.loginscreen.ui.login.LoginPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginModule {
    @NonNull
    @Provides
    public LoginPresenter getLoginPresenter(LoginInteractor loginInteractor){
        return new LoginPresenter(loginInteractor);
    }

    @NonNull
    @Provides
    LoginInteractor provideLogin(SharedPreferences preferences) {
        return new LoginInteractorImpl( preferences );
    }

    @NonNull
    @Provides
    public HomePresenter getHomePresenter(LoginInteractor loginInteractor){
        return new HomePresenter(loginInteractor);
    }
}

