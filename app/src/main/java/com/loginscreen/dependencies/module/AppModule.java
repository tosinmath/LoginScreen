package com.loginscreen.dependencies.module;


import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import com.loginscreen.interactor.LoginInteractor;
import com.loginscreen.interactor.LoginInteractorImpl;
import com.loginscreen.ui.login.LoginPresenter;

import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return mApplication;
    }
}
