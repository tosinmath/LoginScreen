package com.loginscreen.util;

import android.support.annotation.NonNull;
import org.apache.commons.validator.routines.EmailValidator;

public class LoginUtil {

    public static boolean isEmailValid(String email){
        return EmailValidator.getInstance().isValid(email);
    }

    public static boolean isPasswordLenValid(@NonNull String password){
        return password.length() > 5;
    }

    public static boolean isPasswordValid(@NonNull String password){
        if (password == null) {
            return false;
        }
        // Strong password to be used as test : ggH8@w
        String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,}";
        return password.matches(pattern);
    }

}
