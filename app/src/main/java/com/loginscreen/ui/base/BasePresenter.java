package com.loginscreen.ui.base;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class BasePresenter<T extends MvpView> implements MvpPresenter<T> {

    @Nullable
    private T mMvpView;

    @Override
    public void attachView(@NonNull T mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void detachView() {
        mMvpView = null;
    }

    public boolean isViewAttached() {
        return mMvpView != null;
    }

    @Nullable
    public T getMvpView() {
        return mMvpView;
    }

    public void checkViewAttached() {
        if (!isViewAttached()) throw new MvpViewNotAttachedException();
    }

    public static class MvpViewNotAttachedException extends RuntimeException {
        public MvpViewNotAttachedException() {
            super("Please call Presenter.onAttach(MvpView) before requesting data to the Presenter");
        }
    }
}
