package com.loginscreen.ui.login;

import com.loginscreen.ui.base.MvpView;

public interface LoginView extends MvpView {

    void launchHomeActivity();

    void displaySuccessMsg();
}
