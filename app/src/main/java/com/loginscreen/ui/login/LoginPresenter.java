package com.loginscreen.ui.login;

import android.support.annotation.NonNull;
import android.util.Log;
import com.loginscreen.interactor.LoginInteractor;
import com.loginscreen.ui.base.BasePresenter;

public class LoginPresenter extends BasePresenter<LoginView> {

    private LoginInteractor loginInteractor;
    private LoginView loginView;

    public LoginPresenter(LoginInteractor loginInteractor) {
        this.loginInteractor = loginInteractor;
    }

    @Override
    public void attachView(LoginView loginView){
        super.attachView(loginView);
    }

    @Override
    public void detachView(){
        super.detachView();
    }

    public void userLogin(String email, String password){
        if(isViewAttached()) {
            if(loginInteractor.saveLoginStatus(email, password)) {
                getMvpView().displaySuccessMsg();
                getMvpView().launchHomeActivity();
            }
        }
    }
}
