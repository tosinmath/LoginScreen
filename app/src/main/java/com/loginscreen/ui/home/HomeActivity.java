package com.loginscreen.ui.home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.loginscreen.BaseApplication;
import com.loginscreen.R;
import com.loginscreen.ui.base.BaseActivity;
import com.loginscreen.ui.login.LoginActivity;
import com.loginscreen.ui.login.LoginPresenter;
import com.loginscreen.ui.login.LoginView;

import javax.inject.Inject;

public class HomeActivity extends BaseActivity implements HomeView {

    @Inject
    HomePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ((BaseApplication) getApplication()).getLoginComponent().inject(this);
        presenter.attachView(this);

        // fetch user email and display
        presenter.getUserEmail();

        Button button = findViewById(R.id.logout_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutUser();
            }
        });
    }

    public void logoutUser(){
        presenter.clearUserRecords();
    }

    public void launchLoginScreen(){
        showToast(getResources().getString(R.string.logout_success));
        Context context = getApplicationContext();
        Intent intent = new Intent(context, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        finish();
    }

    public void displayWelcome(String str){
        String text = getResources().getString(R.string.welcome_text) + " " + str;
        TextView welcome = findViewById(R.id.welcome);
        welcome.setText(text);
    }
}
