package com.loginscreen.ui.home;


import com.loginscreen.ui.base.MvpView;

public interface HomeView extends MvpView {
    void logoutUser();
    void displayWelcome(String str);
    void launchLoginScreen();
}
