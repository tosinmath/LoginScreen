package com.loginscreen.ui.home;


import android.util.Log;

import com.loginscreen.interactor.LoginInteractor;
import com.loginscreen.ui.base.BasePresenter;
import com.loginscreen.ui.login.LoginView;


public class HomePresenter extends BasePresenter<HomeView> {

    private LoginInteractor loginInteractor;
    private HomeView homeView;

    public HomePresenter(LoginInteractor loginInteractor) {
        this.loginInteractor = loginInteractor;
    }

    @Override
    public void attachView(HomeView homeView){
        super.attachView(homeView);
    }

    @Override
    public void detachView(){
        super.detachView();
    }

    public void getUserEmail(){
        if(isViewAttached()) {
            String str = loginInteractor.userEmail();
            getMvpView().displayWelcome(str);
        }
    }

    public void clearUserRecords(){
        if(isViewAttached()) {
            if(loginInteractor.clearPreferences()) {
                getMvpView().launchLoginScreen();
            }
        }
    }

}

