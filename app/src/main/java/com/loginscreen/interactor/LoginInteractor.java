package com.loginscreen.interactor;

public interface LoginInteractor {

    boolean saveLoginStatus(String email, String password);
    String userEmail();
    boolean clearPreferences();
}
