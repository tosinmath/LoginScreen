package com.loginscreen.interactor;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.loginscreen.R;

import javax.inject.Inject;

public class LoginInteractorImpl implements LoginInteractor {

    private SharedPreferences sharedPreferences;
    private String LOGIN_STATUS = "login_status";
    private String EMAIL = "email";
    private String PASSWORD = "password";
    private String USER = "User";

    @Inject
    public LoginInteractorImpl(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public boolean saveLoginStatus(String email, String password){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(LOGIN_STATUS, 1);
        editor.putString(EMAIL, email);
        editor.putString(PASSWORD, password);
        return editor.commit();
    }

    public String userEmail(){
        return sharedPreferences.
                getString(EMAIL, USER);
    }

    public boolean clearPreferences(){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        return editor.commit();
    }

}


