package com.loginscreen;


import com.loginscreen.util.LoginUtil;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LoginUtilValidEmailTest {
    @Test
    public void isEmail_Valid_ReturnsTrue() {
        assertTrue(LoginUtil.isEmailValid("tosinmath007@gmail.com"));
    }

    @Test
    public void isEmail_SubDomainValid_ReturnsTrue() {
        assertTrue(LoginUtil.isEmailValid("tosinmath007@yahoo.co.uk"));
    }

    @Test
    public void isEmail_Invalid_ReturnsFalse() {
        assertFalse(LoginUtil.isEmailValid("tosinmath007@yahoo"));
    }

    @Test
    public void isEmail_NoDomain_ReturnsFalse() {
        assertFalse(LoginUtil.isEmailValid("tosinmath007@"));
    }

    @Test
    public void isEmail_NoUsername_ReturnsFalse() {


        assertFalse(LoginUtil.isEmailValid("@aol.com"));
    }

    @Test
    public void isEmail_EmptyString_ReturnsFalse() {
        assertFalse(LoginUtil.isEmailValid(""));
    }

    @Test
    public void isEmail_Null_ReturnsFalse() {
        assertFalse(LoginUtil.isEmailValid(null));
    }
}
