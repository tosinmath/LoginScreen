package com.loginscreen;


import com.loginscreen.util.LoginUtil;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LoginUtilValidPasswordTest {
    @Test
    public void isPassword_Valid_ReturnsTrue() {
        assertTrue(LoginUtil.isPasswordValid("ggH8@w"));
    }

    @Test
    public void isPassword_LengthValid_ReturnsTrue() {
        assertTrue(LoginUtil.isPasswordValid("ggH8@w"));
    }

    @Test
    public void isPassword_LengthIsInvalid_ReturnsFalse() {
        assertFalse(LoginUtil.isPasswordValid("ggH8@"));
    }

    @Test
    public void isPassword_ContainsNoNumber_ReturnsFalse() {
        assertFalse(LoginUtil.isPasswordValid("ggH@wk"));
    }

    @Test
    public void isPassword_ContainsNoLetter_ReturnsFalse() {
        assertFalse(LoginUtil.isPasswordValid("567339"));
    }

    @Test
    public void isPassword_EmptyString_ReturnsFalse() {
        assertFalse(LoginUtil.isPasswordValid(""));
    }

    @Test
    public void isPassword_Null_ReturnsFalse() {
        assertFalse(LoginUtil.isPasswordValid(null));
    }
}
